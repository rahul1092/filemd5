package rahul;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class FileMD5 
{
	private static String destinationFolder = "C:\\Users\\rahuls750\\Documents\\duplicate\\";
	
	private ArrayList<String> fileList;
	private HashMap<String, String> md5Map;

	//Constructor	
	public FileMD5()
	{
		fileList = new ArrayList<String>();
		md5Map = new HashMap<String, String>();
	}
	
	public static void main(String[] args) throws NoSuchAlgorithmException 
	{
		if (args[0] == null || args[0].equals(""))
		{
			System.err.println("Error : Folder Name is Mandatory");
			System.exit(1);
		}
		FileMD5 obj = new FileMD5();
		obj.populateFileList(args[0]);
		obj.buildMD5Map();
		obj.moveDuplicate();
	}
	
	//Fills in the fileList ArrayList
	private void populateFileList(String folderName)
	{
		File folder = new File(folderName);
		if (!folder.isDirectory())
		{
			System.err.println("Error : Invalid Folder Name");
			System.exit(1);
		}
		File[] fileList = folder.listFiles();
		for (File file : fileList)
		{
			if (file.isFile())
				this.fileList.add(file.getAbsolutePath());
		}
	}
	
	private void buildMD5Map() throws NoSuchAlgorithmException
	{
		Iterator<String> it = this.fileList.iterator();
		byte[] byteArr = new byte[10000];
		MessageDigest md = MessageDigest.getInstance("MD5");
		while (it.hasNext())
		{
			try 
			{
				File file = new File(it.next());
				FileInputStream fileStream = new FileInputStream(file);
				
				//Reads a File as a byte sequence
				while (fileStream.read(byteArr)!=-1)
				{
					md.update(byteArr);
				}
				
				//The md5Map HashMap will contain the File name and the corressponding MD5 Hash
				this.md5Map.put(file.getAbsolutePath(), new BigInteger(1, md.digest()).toString(16));
				fileStream.close();
				file = null;
			}
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	private void moveDuplicate()
	{
		HashSet<String> hashSet = new HashSet<String>();
		String md5Hash = "";
		for (String fileName : md5Map.keySet())
		{
			md5Hash = md5Map.get(fileName);
			if (!hashSet.contains(md5Hash))
			{
				hashSet.add(md5Hash);
			}
			else
			{
				moveDuplicateFile(fileName);
			}
		}
	}
	
	private void moveDuplicateFile(String fileName) 
	{
		//The seperator \ works in the case of Windows OS only
		String destinationStr = destinationFolder + fileName.substring(fileName.lastIndexOf("\\")+1);
		try 
		{
			Files.move(Paths.get(fileName), Paths.get(destinationStr), StandardCopyOption.REPLACE_EXISTING);
			System.out.println("File "+fileName+ " moved to "+ destinationStr);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
